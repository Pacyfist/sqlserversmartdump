﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using SQLServerSmartDump.Dialogs;
using SQLServerSmartDump.LocalStorage;
using SQLServerSmartDump.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SQLServerSmartDump.ViewModels.Windows
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private ConfigFile config = ConfigFile.GetInstance();

        BackgroundWorker worker;

        public MainWindowViewModel()
        {
            NotifyPropertyChanged(() => Databases);

            NotifyPropertyChanged(() => Location);
            NotifyPropertyChanged(() => Server);
            NotifyPropertyChanged(() => Login);
            NotifyPropertyChanged(() => Password);
            NotifyPropertyChanged(() => Database);


            worker = new BackgroundWorker();
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.WorkerReportsProgress = true;
        }



        public string Location
        {
            get { return config.Location; }
            set
            {
                config.Location = value;

                NotifyPropertyChanged(() => Location);
            }
        }

        public string Server
        {
            get { return config.Server; }
            set
            {
                config.Server = value;

                NotifyPropertyChanged(() => Server);
                NotifyPropertyChanged(() => Databases);
            }
        }

        public string Login
        {
            get { return config.Login; }
            set
            {
                config.Login = value;

                NotifyPropertyChanged(() => Server);
                NotifyPropertyChanged(() => Databases);
            }
        }

        public string Password
        {
            get { return config.Password; }
            set
            {
                config.Password = value;

                NotifyPropertyChanged(() => Server);
                NotifyPropertyChanged(() => Databases);
            }
        }

        public string Database
        {
            get { return config.Database; }
            set
            {
                config.Database = value;
            }
        }

        public IList<string> Databases
        {
            get
            {
                IList<string> result = new List<string>();

                if (!string.IsNullOrEmpty(config.Server) && !string.IsNullOrEmpty(config.Login) && !string.IsNullOrEmpty(config.Password))
                {
                    using (SqlConnection connection = new SqlConnection(string.Format("Server={0};User Id={1};Password={2};Connection Timeout=1", config.Server, config.Login, config.Password)))
                    {
                        try
                        {
                            connection.Open();
                            if (connection.State == ConnectionState.Open)
                            {
                                foreach (Database database in new Server(new ServerConnection(connection)).Databases)
                                {
                                    result.Add(database.Name);
                                }
                            }
                        }
                        catch (SqlException) { }
                    }
                }

                return result;
            }

        }

        public void ChangeLocation()
        {
            config.Location = FolderDialog.Open();

            NotifyPropertyChanged(() => Location);
        }


        public void Dump()
        {
            worker.RunWorkerAsync();
        }



        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            string tbPath = Path.Combine(config.Location, config.Database, "TableSchema");

            if (Directory.Exists(tbPath))
                Directory.Delete(tbPath, true);

            if (!Directory.Exists(tbPath))
                Directory.CreateDirectory(tbPath);

            string vwPath = Path.Combine(config.Location, config.Database, "Views");

            if (Directory.Exists(vwPath))
                Directory.Delete(vwPath, true);

            if (!Directory.Exists(vwPath))
                Directory.CreateDirectory(vwPath);

            string spPath = Path.Combine(config.Location, config.Database, "StoredProcedures");

            if (Directory.Exists(spPath))
                Directory.Delete(spPath, true);

            if (!Directory.Exists(spPath))
                Directory.CreateDirectory(spPath);

            string fnPath = Path.Combine(config.Location, config.Database, "Functions");

            if (Directory.Exists(fnPath))
                Directory.Delete(fnPath, true);

            if (!Directory.Exists(fnPath))
                Directory.CreateDirectory(fnPath);

            ServerConnection conn = new ServerConnection(config.Server, config.Login, config.Password);
            try
            {
                Server server = new Server(conn);

                Database db = server.Databases[config.Database];

                int i = 0;
                int i_max = db.Tables.Count + db.Views.Count + db.StoredProcedures.Count + db.UserDefinedFunctions.Count;


                foreach (Table tb in db.Tables)
                {
                    i++;
                    worker.ReportProgress(100 * i / i_max);

                    if (tb.IsSystemObject == true)
                        continue;

                    string tbSchemaPath = Path.Combine(tbPath, tb.Schema);

                    if (!Directory.Exists(tbSchemaPath))
                        Directory.CreateDirectory(tbSchemaPath);

                    ScriptingOptions options = new ScriptingOptions();
                    options.IncludeIfNotExists = true;
                    options.NoCommandTerminator = false;
                    options.ToFileOnly = true;
                    options.AllowSystemObjects = false;
                    options.Permissions = true;
                    options.DriAllConstraints = true;
                    options.SchemaQualify = true;
                    options.AnsiFile = true;
                    options.SchemaQualifyForeignKeysReferences = true;
                    options.Indexes = true;
                    options.DriIndexes = true;
                    options.DriClustered = true;
                    options.DriNonClustered = true;
                    options.NonClusteredIndexes = true;
                    options.ClusteredIndexes = true;
                    options.FullTextIndexes = true;
                    options.EnforceScriptingOptions = true;
                    options.IncludeHeaders = true;
                    options.SchemaQualify = true;
                    options.NoCollation = true;
                    options.DriAll = true;
                    options.DriAllKeys = true;
                    options.ToFileOnly = true;
                    options.NoExecuteAs = true;
                    options.AppendToFile = false;
                    options.ToFileOnly = false;
                    options.Triggers = true;
                    options.IncludeDatabaseContext = false;
                    options.AnsiPadding = true;
                    options.FullTextStopLists = true;
                    options.ScriptBatchTerminator = true;
                    options.ExtendedProperties = true;
                    options.FullTextCatalogs = true;
                    options.XmlIndexes = true;
                    options.ClusteredIndexes = true;
                    options.Default = true;
                    options.DriAll = true;
                    options.Indexes = true;
                    options.IncludeHeaders = true;
                    options.ExtendedProperties = true;
                    options.WithDependencies = false;

                    StringBuilder tbScriptLines = new StringBuilder();
                    foreach (string line in tb.Script(options))
                        tbScriptLines.Append(line);

                    string tbScriptPath = Path.Combine(tbSchemaPath, tb.Name + ".sql");
                    string tbScriptContent = tbScriptLines.ToString();
                    string tbFormattedScriptContent = TSQLFormatter.Format(tbScriptContent);

                    File.WriteAllText(tbScriptPath, tbFormattedScriptContent);
                }

                foreach (View vw in db.Views)
                {
                    i++;
                    worker.ReportProgress(100 * i / i_max);

                    if (vw.IsSystemObject == true)
                        continue;

                    string vwSchemaPath = Path.Combine(vwPath, vw.Schema);

                    if (!Directory.Exists(vwSchemaPath))
                        Directory.CreateDirectory(vwSchemaPath);

                    ScriptingOptions options = new ScriptingOptions();
                    options.ClusteredIndexes = true;
                    options.Default = true;
                    options.FullTextIndexes = true;
                    options.Indexes = true;
                    options.NonClusteredIndexes = true;
                    options.SchemaQualify = true;
                    options.ScriptData = false;
                    options.ScriptDrops = false;
                    options.ScriptSchema = true;
                    options.Statistics = true;
                    options.Triggers = true;
                    options.WithDependencies = false;
                    options.DriAll = true;
                    options.IncludeHeaders = false;


                    StringBuilder vwScriptLines = new StringBuilder();
                    foreach (string line in vw.Script(options))
                        vwScriptLines.Append(line);

                    string vwScriptPath = Path.Combine(vwSchemaPath, vw.Name + ".sql");
                    string vwScriptContent = vwScriptLines.ToString();
                    string vwFormattedScriptContent = TSQLFormatter.Format(vwScriptContent);

                    File.WriteAllText(vwScriptPath, vwFormattedScriptContent);
                }


                foreach (StoredProcedure sp in db.StoredProcedures)
                {
                    i++;
                    worker.ReportProgress(100 * i / i_max);

                    if (sp.IsSystemObject == true)
                        continue;

                    string spSchemaPath = Path.Combine(spPath, sp.Schema);

                    if (!Directory.Exists(spSchemaPath))
                        Directory.CreateDirectory(spSchemaPath);

                    string spScriptPath = Path.Combine(spSchemaPath, sp.Name.Trim() + ".sql");
                    string spScriptContent = sp.ScriptHeader(true) + sp.TextBody;

                    string spFormattedScriptContent = TSQLFormatter.Format(spScriptContent);

                    File.WriteAllText(spScriptPath, spFormattedScriptContent);
                }


                foreach (UserDefinedFunction fn in db.UserDefinedFunctions)
                {
                    i++;
                    worker.ReportProgress(100 * i / i_max);

                    if (fn.IsSystemObject == true)
                        continue;

                    string fnSchemaPath = Path.Combine(fnPath, fn.Schema);

                    if (!Directory.Exists(fnSchemaPath))
                        Directory.CreateDirectory(fnSchemaPath);

                    string fnScriptPath = Path.Combine(fnSchemaPath, fn.Name + ".sql");
                    string fnScriptContent = fn.ScriptHeader(true) + fn.TextBody;

                    File.WriteAllText(fnScriptPath, fnScriptContent);
                }

                MessageBox.Show("Everything OK!", "Done :)", MessageBoxButton.OK);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message);
            }
            finally
            {
                conn.Disconnect();
                worker.ReportProgress(0);
            }
        }


        void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Progress = e.ProgressPercentage;
        }


        private int _progress = 0;
        private int _progressMin = 0;
        private int _progressMax = 100;

        public int Progress
        {
            get
            {
                return _progress;
            }
            set
            {
                _progress = value;

                NotifyPropertyChanged(() => Progress);
            }
        }

        public int ProgressMin
        {
            get
            {
                return _progressMin;
            }
            set
            {
                _progressMin = value;

                NotifyPropertyChanged(() => ProgressMin);
            }
        }

        public int ProgressMax
        {
            get
            {
                return _progressMax;
            }
            set
            {
                _progressMax = value;

                NotifyPropertyChanged(() => ProgressMax);
            }
        }


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged<T>(Expression<Func<T>> selectorExpression)
        {
            if (selectorExpression == null)
                throw new ArgumentNullException("selectorExpression");

            MemberExpression body = selectorExpression.Body as MemberExpression;
            if (body == null)
                throw new ArgumentException("The body must be a member expression");

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(body.Member.Name));
            }
        }
        #endregion

    }
}
