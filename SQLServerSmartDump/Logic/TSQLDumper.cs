﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SQLServerSmartDump.Logic
{
    public class TSQLDumper
    {
        public event EventHandler CountUpdate;
        protected virtual void OnCountUpdate()
        {
            if (CountUpdate != null)
            {
                CountUpdate(this, null);
            }
        }


        private string _server;
        private string _login;
        private string _password;
        private string _database;

        public TSQLDumper(string server, string login, string password, string database)
        {
            this._server = server;
            this._login = login;
            this._password = password;
            this._database = database;
        }

        public void Dump(string path)
        {
            string spPath = Path.Combine(path, _database, "StoredProcedures");

            if (Directory.Exists(spPath))
                Directory.Delete(spPath, true);

            if (!Directory.Exists(spPath))
                Directory.CreateDirectory(spPath);

            string tbPath = Path.Combine(path, _database, "TableSchema");

            if (Directory.Exists(tbPath))
                Directory.Delete(tbPath, true);

            if (!Directory.Exists(tbPath))
                Directory.CreateDirectory(tbPath);

            string fnPath = Path.Combine(path, _database, "Functions");

            if (Directory.Exists(fnPath))
                Directory.Delete(fnPath, true);

            if (!Directory.Exists(fnPath))
                Directory.CreateDirectory(fnPath);

            ServerConnection conn = new ServerConnection(this._server, this._login, this._password);
            try
            {
                Server server = new Server(conn);

                Database db = server.Databases[this._database];

                int i = 0;

                foreach (Table tb in db.Tables)
                {
                    i++;

                    if (tb.IsSystemObject == true)
                        continue;

                    string tbSchemaPath = Path.Combine(tbPath, tb.Schema);

                    if (!Directory.Exists(tbSchemaPath))
                        Directory.CreateDirectory(tbSchemaPath);

                    ScriptingOptions options = new ScriptingOptions();
                    options.ClusteredIndexes = true;
                    options.Default = true;
                    options.FullTextIndexes = true;
                    options.Indexes = true;
                    options.NonClusteredIndexes = true;
                    options.SchemaQualify = true;
                    options.ScriptData = false;
                    options.ScriptDrops = false;
                    options.ScriptSchema = true;
                    options.Statistics = true;
                    options.Triggers = true;
                    options.WithDependencies = false;
                    options.DriAll = true;
                    options.IncludeHeaders = false;


                    using (StreamWriter writer = new StreamWriter(Path.Combine(tbSchemaPath, tb.Name + ".sql")))
                    {
                        foreach (string line in tb.Script(options))
                        {
                            writer.WriteLine(line);
                        }

                        writer.Close();
                    }
                }

                foreach (StoredProcedure sp in db.StoredProcedures)
                {
                    i++;

                    if (sp.IsSystemObject == true)
                        continue;

                    string spSchemaPath = Path.Combine(spPath, sp.Schema);

                    if (!Directory.Exists(spSchemaPath))
                        Directory.CreateDirectory(spSchemaPath);

                    string spScriptPath = Path.Combine(spSchemaPath, sp.Name + ".sql");
                    string spScriptContent = sp.ScriptHeader(true) + sp.TextBody;

                    string spFormattedScriptContent = TSQLFormatter.Format(spScriptContent);

                    File.WriteAllText(spScriptPath, spFormattedScriptContent);
                }


                foreach (UserDefinedFunction fn in db.UserDefinedFunctions)
                {
                    i++;

                    if (fn.IsSystemObject == true)
                        continue;

                    string fnSchemaPath = Path.Combine(fnPath, fn.Schema);

                    if (!Directory.Exists(fnSchemaPath))
                        Directory.CreateDirectory(fnSchemaPath);

                    string fnScriptPath = Path.Combine(fnSchemaPath, fn.Name + ".sql");
                    string fnScriptContent = fn.ScriptHeader(true) + fn.TextBody;
                    File.WriteAllText(fnScriptPath, fnScriptContent);
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Disconnect();
            }

        }
    }
}
