﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PoorMansTSqlFormatterLib;
using PoorMansTSqlFormatterLib.Tokenizers;
using PoorMansTSqlFormatterLib.Parsers;
using PoorMansTSqlFormatterLib.Formatters;

namespace SQLServerSmartDump.Logic
{
    public static class TSQLFormatter
    {
        static TSqlStandardTokenizer tokenizer = new TSqlStandardTokenizer();
        static TSqlStandardParser parser = new TSqlStandardParser();
        static TSqlStandardFormatter formatter = new TSqlStandardFormatter();

        public static string Format(string inScript)
        {
            string outScript = string.Empty;

            var tokenList = tokenizer.TokenizeSQL(inScript);

            var parsed = parser.ParseSQL(tokenList);

            outScript = formatter.FormatSQLTree(parsed);

            return outScript;
        }
    }
}
