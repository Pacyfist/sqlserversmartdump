﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLServerSmartDump.LocalStorage
{
    public class ConfigFile
    {
        private Logger log = LogManager.GetCurrentClassLogger();

        private ConfigFile()
        {
            Load();
        }

        #region Singleton
        private static ConfigFile _instance = null;

        public static ConfigFile GetInstance()
        {
            if (_instance == null)
                _instance = new ConfigFile();

            return _instance;
        }
        #endregion

        private const string _ConfigurationFolderName = "SQLServerSmartDump";
        private const string _ConfigurationFileName = "ConfigFile.conf";

        private string _ConfigurationFilePath = null;

        private string ConfigurationFilePath
        {
            get
            {
                if (_ConfigurationFilePath == null)
                {
                    _ConfigurationFilePath = GetConfigurationFilePath(_ConfigurationFolderName, _ConfigurationFileName);
                }

                return _ConfigurationFilePath;
            }
        }

        private string _Location = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), _ConfigurationFolderName);
        private string _Server = @".\SQLEXPRESS";
        private string _Login = @"sa";
        private string _Password = @"StandardE80";
        private string _Database = @"";

        public string Location
        {
            get { return _Location; }
            set
            {
                _Location = value;
                Save();
            }
        }
        public string Server
        {
            get { return _Server; }
            set
            {
                _Server = value;
                Save();
            }
        }
        public string Login
        {
            get { return _Login; }
            set
            {
                _Login = value;
                Save();
            }
        }
        public string Password
        {
            get { return _Password; }
            set
            {
                _Password = value;
                Save();
            }
        }
        public string Database
        {
            get { return _Database; }
            set
            {
                _Database = value;
                Save();
            }
        }

        private string GetConfigurationFilePath(string _ConfigurationFolderName, string _ConfigurationFileName)
        {
            string appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            string _ConfigurationFolderPath = Path.Combine(appdata, _ConfigurationFolderName);
            string _ConfigurationFilePath = Path.Combine(appdata, _ConfigurationFolderPath, _ConfigurationFileName);

            SetupConfigurationFolder(_ConfigurationFolderPath);
            SetupConfigurationFile(_ConfigurationFilePath);

            CleanConfigurationFolder(_ConfigurationFolderPath, _ConfigurationFilePath);

            return _ConfigurationFilePath;
        }

        private void SetupConfigurationFolder(string _ConfigurationFolderPath)
        {
            if (!Directory.Exists(_ConfigurationFolderPath))
            {
                Directory.CreateDirectory(_ConfigurationFolderPath);
            }
        }

        private void SetupConfigurationFile(string _ConfigurationFilePath)
        {
            if (!File.Exists(_ConfigurationFilePath))
            {
                Save(_ConfigurationFilePath);
            }
        }

        private void CleanConfigurationFolder(string _ConfigurationFolderPath, string _ConfigurationFilePath)
        {
            foreach (string path in Directory.EnumerateFiles(_ConfigurationFolderPath))
            {
                if (!_ConfigurationFilePath.Equals(path))
                {
                    try
                    {
                        File.Delete(path);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                    }
                }
            }
        }


        public void Load(string _ConfigurationFilePath = null)
        {
            if (_ConfigurationFilePath == null)
                _ConfigurationFilePath = this.ConfigurationFilePath;

            using (StreamReader reader = new StreamReader(_ConfigurationFilePath))
            {
                this._Location = reader.ReadLine();
                this._Server = reader.ReadLine();
                this._Login = reader.ReadLine();
                this._Password = reader.ReadLine();
                this._Database = reader.ReadLine();
            }
        }

        public void Save(string _ConfigurationFilePath = null)
        {
            if (_ConfigurationFilePath == null)
                _ConfigurationFilePath = this.ConfigurationFilePath;

            using (StreamWriter writer = new StreamWriter(_ConfigurationFilePath))
            {
                writer.WriteLine(this._Location);
                writer.WriteLine(this._Server);
                writer.WriteLine(this._Login);
                writer.WriteLine(this._Password);
                writer.WriteLine(this._Database);
            }
        }
    }
}
